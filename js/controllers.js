(function () {
    'use strict';
		
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-bottom-full-width",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    /* Controllers */

    // Global "database"
    var itemList = [];

    var itemsControllers = angular.module('itemsControllers', []);
		
    itemsControllers.controller('MediaList', ['$scope', '$http','$location',
        function ($scope, $http, $location) {	
            $scope.items = {};
               
            $scope.load = function() {
                $http.get('/wordlog/index.php/media/index/').success( function(data, status, headers, config) {
                    $scope.items = data;
                }).error(function() {
					$scope.items = {}
				})
				
				$http.get('/wordlog/index.php/media/types/').then(function(response){
					$scope.mediaTypes = sortType(response.data);
				})
               
            }
            
            $scope.newMedia = function() {
                $http.post('/wordlog/index.php/media/new', {name : $scope.newMediaName, type : $scope.newMediaType}).then(function() {
                    $scope.load();
                })
            }
            
            $scope.deleteMedia = function(id) {
				$http.delete('/wordlog/index.php/media/delete/id/' + id).then(function(response) {
					console.log(response)
					$scope.load();
					});
				
			}
			
			var sortType = function(types) {
				var orderedTypes = new Array();
				for (var i = 0; i < types.length; i++) {
					var type = types[i];
					type.children = new Array();
					
					
					for(var j = 0; j < types.length; j++) {
						if (types[j].parentid == type.id){
							types[j].typename = type.typename + " >> " + types[j].typename;
						}; 
					}
					console.log(type);
						//orderedTypes.push(type);
				}
				
				console.log(orderedTypes);
				return types;
		}
            
            $scope.load();
        }]);
		
	itemsControllers.controller('MediaListView', ['$scope', '$http','$routeParams',
        function ($scope, $http, $routeParams) {	
            
			var mediaId = $routeParams.mediaId;
			$scope.load = function(){
				console.log('loading lists')
				 $http.get('/wordlog/index.php/media/list/mediaid/' + mediaId).then( function(response) {
					
                    $scope.lists = response.data;
                    //$scope.storedWords = response.data.words;  
                })
			}
			
			$scope.newList = function(){
				var d = {
                    name : $scope.newListName
                };
				$http.post('/wordlog/index.php/media/list/mediaid/' + mediaId, d).then(function() {
                    console.log('done');
                    $scope.load()
                })
			}
			
			$scope.wordSearch = function(word) {
                //console.log('hello');
                return $http.get('/wordlog/index.php/word/index/queryString/' + word)
                .then(function(result) {
                    //console.log(result.data);
                    return result.data;
                })
            }
            
            $scope.storeWord = function(item, model, label) {
                var d = {
                    word : item
                };
                $http.post('/wordlog/index.php/media/words/id/' + listId, d)
                .then(function() {
                    $scope.loadPage();
                    $('form > input').val('');
                });
                
            }
			
            $scope.load();
        }]);
		
    itemsControllers.controller('MediaEdit', ['$scope', '$http', '$routeParams', 
        function ($scope, $http, $routeParams) {
           
            var mediaId = $routeParams.mediaId;
			var listId = $routeParams.listId;
            $scope.loadPage = function() {
            $http.get('/wordlog/index.php/media/index/id/' + mediaId).then( function(response) {
                    $scope.media = response.data;         
            });
			$http.get('/wordlog/index.php/media/list/mediaid/' + mediaId + '/listid/' + listId).then( function(response) {
					
					console.log(response.data);
					console.log('/wordlog/index.php/media/list/mediaid/' + mediaId + '/listid/' + listId)
                    $scope.storedWords = response.data.words; 
                     
                });
				
               
            }
            
           
            $scope.wordSearch = function(word) {
                //console.log('hello');
                return $http.get('/wordlog/index.php/word/index/queryString/' + word)
                .then(function(result) {
                    //console.log(result.data);
                    return result.data;
                })
            }
            
            $scope.storeWord = function(item, model, label) {
                var d = {
                    word : item
                };
                $http.post('/wordlog/index.php/media/words/id/' + listId, d)
                .then(function() {
                    $scope.loadPage();
                    $('form > input').val('');
                });
                
            }
            
            $scope.deleteWord = function(id) {
                console.log('deleting word of id: ' + id);
                $http.delete('/wordlog/index.php/media/words/id/' + listId + '/wordId/' + id).then(function() {
                    console.log('done');
                    $scope.loadPage()
                })
            }
            
            
            $scope.loadPage();
                
        }]);
}())
