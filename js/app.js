(function () {
    'use strict';

    /* App Module */

    var itemsApp = angular.module('itemsApp', [
        'ngRoute',
        'itemsControllers',
        'ui.bootstrap'
        ]);
       

    itemsApp.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
            when('/media', {
                templateUrl: '/wordlog/application/views/app/partials/media-list.html',
                controller: 'MediaList'
            }).
            when('/media/:mediaId', {
                templateUrl: '/wordlog/application/views/app/partials/media-listview.html',
                controller: 'MediaListView'
            }).
			when('/media/:mediaId/list/:listId', {
                templateUrl: '/wordlog/application/views/app/partials/list-edit.html',
                controller: 'MediaEdit'
            }).			  
            otherwise({
                redirectTo: '/media'
            });
        }]);
}())
