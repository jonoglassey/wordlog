<?php

class WordList extends CI_Model {

    private $tableName;
    public $listid;
	public $mediaid;
    public $name;
    public $words;

	
	public function __construct() {
        $this->load->database();
        $this->tableName = "lists";
        $this->tableMaps = array();
    }
   

    public function read($listId) {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('lists.listid', $listId);
		$this->db->where('lists.parentlistid IS NOT NULL', NULL);
		//$this->db->where('lists.mediaid', $mediaId);
		
        $query = $this->db->get();


        if ($query->num_rows === 0) {
            //exit("exception");
            throw new Exception("Media Item of ID $id not found in Database");
        }

        $media = new WordList();
        $result = $query->result();

        //exit(var_dump($query->result()));
        $row = $result[0];

        $words = $this->getWords($listId);

        $media->load($row, $words);
		//exit(var_dump($media));

        return $media;
    }
	
	
	public function getMediaLists($mediaId) {
		$lists = array();
		$this->db->select('*');
        $this->db->from($this->tableName);
        //$this->db->where('lists.listid', $listId);
		$this->db->where('lists.mediaid', $mediaId);
		$this->db->where('lists.parentlistid IS NOT NULL', NULL);
		
        $query = $this->db->get();
		if ($query->num_rows !== 0) {
			$result = $query->result();
			
			foreach($result as $r) {
				$lists[] = $this->read($r->listid);
			}
			
			
		}
		return $lists;
        //exit(var_dump($query->result()));
	}
	
	private function load($row, $words) {
        $this->listid = $row->listid;
		$this->mediaid = $row->mediaid;
        $this->name = mysql_real_escape_string($row->listname);
        $this->mediaid = $row->mediaid;
        $this->words = array();
		
        foreach ($words as $id => $word) {
            $this->words[$id] = $word;
        }
		
    }
	
	    public function getWords($id) {
        $words = array();
        $this->db->select('*');
        $this->db->from('mediawordmap');
        $this->db->where('mediaid', $id);
        $this->db->join('words', 'mediawordmap.wordid = words.id', 'right');

        $query = $this->db->get();
        $result = $query->result();

        foreach ($result as $row) {
            $definition = $this->getDefinitions($row);
            $words[$row->id] = $definition;
            //echo $row->word;
        }

        //exit(var_dump($result));
        return $words;
    }
	
	    public function getDefinitions($row) {
        //exit();
        $string = $row->word;

        $hiraganaRegex = '/\[\W*\]/';
        $descRegex = '/\/.*\//'; //Result must be split on '/' character
        $kanjiRegex = '/([\S\-]+)/';

        preg_match($hiraganaRegex, $string, $hiragana);
        preg_match($kanjiRegex, $string, $word);
        preg_match($descRegex, $string, $descString);
        $desc = explode('/', $descString[0]);
        $words = explode(';', $word[0]);

        $description = array();
        
        foreach ($desc as $d) {
            if ($d !== '') {
                $descriptions[] = $d;
            }
        }
        
        //var_dump($hiragana);
        
        $def = array();
        $def['word'] = $words;
        $def['hiragana'] = empty($hiragana) ? array() : $hiragana[0];
		$def['hiragana'] = str_replace(array('[', ']'),'', $def['hiragana']);
        $def['defintions'] = $descriptions;
        
        return $def;
    }

}
