<?php

class MediaItem extends CI_Model {

    private $tableName;
    public $id;
    public $name;
    public $type;
    public $words;
	public $lists;

    public function __construct() {
        $this->load->database();
        $this->tableName = "media";
        $this->tableMaps = array();
		$this->lists = array();
		$this->load->model('wordlist');
        $this->tableMaps['mediatypes'] = 'mediatype';
    }

    public function read($id) {
        $this->db->select('*');
        $this->db->from($this->tableName);
        //$this->db->from('words');
        $this->db->where('media.id', $id);
		$this->db->where('lists.parentlistid', null);
		 
		//$this->db->join($this->tableMaps['mediatypes'], 'media.id = mediatype.id','full');
        $this->db->join('lists', 'media.id = lists.mediaid', 'left');
        //$this->db->join('mediawordmap','mediawordmap.wordid = words.id', 'full');
        //$this->db->join('mediawordmap', 'words.id = mediawordmap.wordid','left');
        //$this->db->order_by('order');

        $query = $this->db->get();


        if ($query->num_rows === 0) {
            //exit("exception");
            throw new Exception("Media Item of ID $id not found in Database");
        }

        $media = new MediaItem();
        $result = $query->result();

        //exit(var_dump($query->result()));
        $row = $result[0];

        //$words = $this->getWords($id);

        $media->load($row);
		//exit(var_dump($media));

        return $media;
    }


    public function getAll($sort = null) {
        $polls = array();
        $this->db->select('*');
        $this->db->from($this->tableName);

        $query = $this->db->get();
        $result = $query->result();
        //exit(var_dump($query->result()));

        if ($query->num_rows() <= 0) {
            throw new Exception("No rows present in Database");
        }

        foreach ($result as $row) {
            //echo $i;
            $polls[] = $this->read($row->id);
        }
        //exit($query->num_rows());
        //exit(var_dump($polls));
        //if ($sort != null) {
		//	$polls = $this->groupMedia($polls, $sort);
		//}
        
        return $polls;
    }
    
    private function groupMedia($media, $group) {
		$sortedList = array();
		if ($group == 'type') {
			$types = $this->getTypes();
			foreach($types as $type) {
				foreach($media as $m) {
					if($type->id == $m->type) {
						$sortedList[$type->typename] = $m;	
					}
				}
			}
		}
		
		return $sortedList;
	}

    private function load($row) {
        $this->id = $row->id;
        $this->name = mysql_real_escape_string($row->name);
        $this->type = $this->getType($row->type);
        $this->words = array();
		//exit(var_dump($row));
		$this->parentListId = $row->listid;
		$this->words = $this->getWords($row->listid);
		
		$this->lists = $this->wordlist->getMediaLists($this->id);
		
    }
	
	public function getWords($id) {
        $words = array();
        $this->db->select('*');
        $this->db->from('mediawordmap');
        $this->db->where('mediaid', $id);
        $this->db->join('words', 'mediawordmap.wordid = words.id', 'right');

        $query = $this->db->get();
        $result = $query->result();

        foreach ($result as $row) {
            $definition = $this->getDefinitions($row);
            $words[$row->id] = $definition;
            //echo $row->word;
        }

        //exit(var_dump($result));
        return $words;
    }
	
	public function getDefinitions($row) {
        //exit();
        $string = $row->word;
		$descriptions = array();
		$words = array();
		
        $hiraganaRegex = '/\[\W*\]/';
        $descRegex = '/\/.*\//'; //Result must be split on '/' character
        $kanjiRegex = '/([\S\-]+)/';

        preg_match($hiraganaRegex, $string, $hiragana);
        preg_match($kanjiRegex, $string, $word);
        if(preg_match($descRegex, $string, $descString)) {
			//var_dump($row->word);
			$desc = explode('/', $descString[0]);
			$words = explode(';', $word[0]);

			
			
			foreach ($desc as $d) {
				if ($d !== '') {
					$descriptions[] = $d;
				}
			}
		}
        
        //var_dump($hiragana);
        
        $def = array();
        $def['word'] = $words;
        $def['hiragana'] = empty($hiragana) ? array() : $hiragana[0];
		$def['hiragana'] = str_replace(array('[', ']'),'', $def['hiragana']);
        $def['defintions'] = $descriptions;
        
        return $def;
    }
	
	private function getType($typeId) {
		$types = $this->getTypes();
		foreach($types as $type) {
			if ($type->id === $typeId) {
				
				return $type;
			}
		}
		
	}
    
    public function getTypes() {
		$this->db->select('*');
        $this->db->from($this->tableMaps['mediatypes']);

        $query = $this->db->get();
        $result = $query->result();
        
        //exit(var_dump($result));
        
        //foreach ($result as $row) 
        //    $types[] = $this->read($row->id);
        //}
        
        
        
        return $result;
	}

}
