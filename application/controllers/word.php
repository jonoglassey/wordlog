<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'application/libraries/Format.php';
require_once 'application/libraries/REST_Controller.php';

class Word extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->output->set_content_type('application/json; charset=utf8');
    }

    function index_get() {
        $this->output->set_header("HTTP/1.1 200 OK");
        $word = $this->get('queryString');
        $word = urlencode($word);
        $results = file_get_contents(htmlentities("http://www.csse.monash.edu.au/~jwb/cgi-bin/wwwjdic.cgi?1ZUJ$word"));
        //var_dump($results);
        $DOM = new DOMDocument;
        libxml_use_internal_errors(true);
        $DOM->loadHTML($results);
        $items = $DOM->getElementsByTagName('pre');
        //exit(var_dump($items->item(0)));
        $arr = explode("\n", $items->item(0)->nodeValue);
        if (sizeof($arr) > 0) {
            
            $this->output->set_output(json_encode($arr));
        }
        else {
            $this->output->set_output(json_encode(array()));
        }
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */