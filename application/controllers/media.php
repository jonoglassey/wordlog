<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'application/libraries/Format.php';
require_once 'application/libraries/REST_Controller.php';

class Media extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mediaitem');
		$this->load->model('wordlist');
        $this->output->set_content_type('application/json; charset=utf8');
    }

    function index_get() {
        $id = $this->get('id');
        //Get method media will be sorted by
        $sort = $this->get('group');
		try {
			if ($id) {
				$media = $this->mediaitem->read($id);
				$this->output->set_header("HTTP/1.1 200 OK");
				$this->output->set_output(json_encode($media));
			} else {
				$media = $this->mediaitem->getAll($sort);
				$this->output->set_header("HTTP/1.1 200 OK");
				$this->output->set_output(json_encode($media));
			}
		} catch (Exception $e) {
			$this->output->set_header("HTTP/1.0 404 Not Found");
            $this->output->set_output($e->getMessage());
		}
    }

    function new_post() {
        $name = $this->post('name');
        $type = $this->post('type');
		try {
			$this->db->insert('media', array('name' => $name, 'type' => $type));
			
			$listData = array(
				'mediaid' => $this->db->insert_id(),
				'listname' => 'default'
			);
			
			$this->db->insert('lists', $listData);
		} catch (Exception $e) {
			$this->output->set_header("HTTP/1.0 404 Not Found");
            $this->output->set_output($e->getMessage());
		}	
    }
    
    function delete_delete() {
		$id = $this->get('id');
		
        $this->db->delete('mediawordmap', array('mediaid' => $id));
        $this->db->delete('media', array('id' => $id));
        
        if ($this->db->affected_rows() == 0) {
            $this->output->set_header("HTTP/1.0 404 Not Found");
            $this->response("Nothing was deleted, Media ID: $id");
        } else {
            $this->output->set_header("HTTP/1.1 200 OK");
            $this->response("Success");
        }
	}

    function words_get() {
        $id = $this->get('id');
        $words = $this->mediaitem->getWords($id);
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_output(json_encode($words));
    }

    function words_post() {
        $id = $this->get('id');
        $newWord = $this->post('word');
		$defaultList = $this->post('defaultList');

        $this->db->select('id');
        $this->db->from('words');
        $this->db->where('word', $newWord);
        $query = $this->db->get();

        if ($query->num_rows === 0) {
            $data = array(
                'word' => $newWord
            );
            $this->db->insert('words', $data);
            $wordid = $this->db->insert_id();
        } else {
            $result = $query->result();
            $row = $result[0];
            $wordid = $row->id;
        }
		
		if ($defaultList) {
			
		}

        $mapData = array(
            'wordid' => $wordid,
            'mediaid' => $id
        );

        $this->db->insert('mediawordmap', $mapData);
		

    }

    function words_delete() {
        $id = $this->get('id');
        $wordId = $this->get('wordId');

        $this->db->delete('mediawordmap', array('wordid' => $wordId, 'mediaid' => $id));

        if ($this->db->affected_rows() == 0) {
            $this->output->set_header("HTTP/1.0 404 Not Found");
            $this->response("Nothing was deleted Word ID: $wordId, Media ID: $id");
        } else {
            $this->output->set_header("HTTP/1.1 200 OK");
            $this->response("Success");
        }
    }
	
	function list_post() {
		$mediaId = $this->get('mediaid');
		$listName = $this->post('name');
		
		$media = $this->mediaitem->read($mediaId);
		
		$mapData = array(
            'mediaid' => $mediaId,
            'listname' => $listName,
			'parentlistid' => $media->parentListId
        );

        $this->db->insert('lists', $mapData);
	}
	
	function list_get() {
		$mediaid = $this->get('mediaid');
		$listid = $this->get('listid');
        //Get method media will be sorted by
        $sort = $this->get('group');
        if ($listid) {
            $media = $this->wordlist->read($listid);
            $this->output->set_header("HTTP/1.1 200 OK");
            $this->output->set_output(json_encode($media));
        } else {
            $media = $this->wordlist->getMediaLists($mediaid);
			$this->output->set_output(json_encode($media));
        }
	}

    function anki_get() {
        $id = $this->get('id');

        if ($id) {
            $media = $this->mediaitem->read($id);
            $ankiFile = $this->makeAnkiDeck($media);
            
            $fileName = $media->name . '-' . date("m-d-y") . '.csv';

            // We'll be outputting a PDF
            $this->output->set_header('Content-type: text/csv');

            // It will be called downloaded.pdf
            $this->output->set_header("Content-Disposition: attachment; filename='$fileName'");

            // The PDF source is in original.pdf
            $output = fopen("php://output", "w");
            fwrite($output, $ankiFile);
            fclose($output);
        } else {
            'fail';
        }
    }

    private function makeAnkiDeck($media) {
        $csv = '';
        
        foreach ($media->words as $word) {
            $definitions = '';
            $hiragana = $word['hiragana'];
            foreach($word['defintions'] as $def) {
                $definitions .= str_replace(',', '/',$def) . ' ';
            }
            if (is_array($hiragana)) {
                $hiragana = implode($hiragana);
            }
            //exit(var_dump($word));
            $csv.=  implode($word['word']) . ',' . 
                    $hiragana . ' ' . 
                    $definitions . "\n";
        }

        return $csv;
    }
    
    public function types_get() {
		$types = $this->mediaitem->getTypes();
		$this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_output(json_encode($types));
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
